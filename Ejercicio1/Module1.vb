﻿Module Module1

    Sub Main()

        Dim value As String
        Dim expresion As String
        Dim newExpresoin As String
        Dim n1 As Integer = 0
        Dim min As Integer = 0
        Dim num1 As Integer = 0
        Dim num2 As Integer = 0

        Do
            Console.WriteLine("")
            Console.Write("---> ")
            value = Console.ReadLine
            Try
                value = value.Replace(" "c, String.Empty)
                value = value.Replace("+", " + ").Replace("*", " * ")
                value = value.Replace("/", " / ").Replace("(", " ( ")
                value = value.Replace(")", " ) ").Replace("-", " - ")
                value = value.Replace("^", " ^ ")
                value = " " & value
                value = value.Replace("  ", " ")
                Dim allValues As String() = value.Split(New [Char]() {" "c, CChar(vbTab)})

                For Each count As String In value
                    If (count = "+" Or count = "*" Or count = "/" Or count = "-" Or count = "^") Then
                        n1 += 1
                    End If
                Next

                For Each count As String In value
                    If (count = "^") Then
                        num1 += 1
                    End If
                    If (count = "*" Or count = "/") Then
                        num2 += 1
                    End If
                Next

                For count As Integer = 0 To n1
                    Try
                        For count2 As Integer = 0 To allValues.Length - 1
                            allValues = value.Split(New [Char]() {" "c, CChar(vbTab)})
                            If (allValues(count2) = "^") Then
                                num1 -= 1
                                If (allValues(count2 + 1) = "-" And allValues(count2 - 2) <> "-") Then
                                    expresion = allValues(count2 - 1) & allValues(count2) & allValues(count2 + 1) & allValues(count2 + 2)
                                    value = value.Replace(expresion.Replace("^", " ^ ").Replace(" -", " - "), Expo(expresion))
                                End If
                                If (allValues(count2 - 2) = "-" And allValues(count2 + 1) <> "-") Then
                                    expresion = allValues(count2 - 2) & allValues(count2 - 1) & allValues(count2) & allValues(count2 + 1)
                                    value = value.Replace(expresion.Replace("-", " - ").Replace("^", " ^ "), " " & Expo(expresion).Replace("-", "- "))
                                End If
                                If (allValues(count2 - 2) = "-" And allValues(count2 + 1) = "-") Then
                                    expresion = allValues(count2 - 2) & allValues(count2 - 1) & allValues(count2) & allValues(count2 + 1) & allValues(count2 + 2)
                                    value = value.Replace(expresion.Replace("-", " - ").Replace("^", " ^ ").Replace("  ", " "), Expo(expresion).Replace("-", " - "))
                                End If
                                If (allValues(count2 + 1) <> "-" And allValues(count2 - 2) <> "-") Then
                                    expresion = allValues(count2 - 1) & allValues(count2) & allValues(count2 + 1)
                                    value = value.Replace(expresion.Replace("^", " ^ "), Expo(expresion))
                                End If

                            ElseIf (allValues(count2) = "*" And num1 <= 0) Then
                                num2 -= 1
                                If (allValues(count2 - 2) = "-" And allValues(count2 + 1) <> "-") Then
                                    expresion = allValues(count2 - 2) & allValues(count2 - 1) & allValues(count2) & allValues(count2 + 1)
                                    value = value.Replace(expresion.Replace("-", " - ").Replace("*", " * "), Mult(expresion).Replace("-", " - "))
                                End If
                                If (allValues(count2 - 2) = "-" And allValues(count2 + 1) = "-") Then
                                    expresion = allValues(count2 - 2) & allValues(count2 - 1) & allValues(count2) & allValues(count2 + 1) & allValues(count2 + 2)
                                    value = value.Replace(expresion.Replace("-", " - ").Replace("*", " * ").Replace("  ", " "), " " & Mult(expresion))
                                End If
                                If (allValues(count2 + 1) = "-" And allValues(count2 - 2) <> "-") Then
                                    expresion = allValues(count2 - 1) & allValues(count2) & allValues(count2 + 1) & allValues(count2 + 2)
                                    value = value.Replace(expresion.Replace("-", " - ").Replace("*", " * ").Replace("  ", " "), Mult(expresion).Replace("-", "- "))
                                End If
                                If (allValues(count2 + 1) <> "-" And allValues(count2 - 2) <> "-") Then
                                    expresion = allValues(count2 - 1) & allValues(count2) & allValues(count2 + 1)
                                    value = value.Replace(expresion.Replace("*", " * "), Mult(expresion))
                                End If

                            ElseIf (allValues(count2) = "/" And num1 <= 0) Then
                                num2 -= 1
                                If (allValues(count2 - 2) = "-" And allValues(count2 + 1) <> "-") Then
                                    expresion = allValues(count2 - 2) & allValues(count2 - 1) & allValues(count2) & allValues(count2 + 1)
                                    value = value.Replace(expresion.Replace("-", " - ").Replace("/", " / "), Div(expresion).Replace("-", " - "))
                                End If
                                If (allValues(count2 - 2) = "-" And allValues(count2 + 1) = "-") Then
                                    expresion = allValues(count2 - 2) & allValues(count2 - 1) & allValues(count2) & allValues(count2 + 1) & allValues(count2 + 2)
                                    value = value.Replace(expresion.Replace("-", " - ").Replace("/", " / ").Replace("  ", " "), " " & Div(expresion))
                                End If
                                If (allValues(count2 + 1) = "-" And allValues(count2 - 2) <> "-") Then
                                    expresion = allValues(count2 - 1) & allValues(count2) & allValues(count2 + 1) & allValues(count2 + 2)
                                    value = value.Replace(expresion.Replace("-", " - ").Replace("/", " / ").Replace("  ", " "), Div(expresion).Replace("-", "- "))
                                End If
                                If (allValues(count2 + 1) <> "-" And allValues(count2 - 2) <> "-") Then
                                    expresion = allValues(count2 - 1) & allValues(count2) & allValues(count2 + 1)
                                    value = value.Replace(expresion.Replace("/", " / "), Div(expresion))
                                End If

                            ElseIf (allValues(count2) = "+" And num2 <= 0 And num1 <= 0) Then
                                If (allValues(count2 - 2) = "-" And allValues(count2 + 1) <> "-") Then
                                    expresion = allValues(count2 - 2) & allValues(count2 - 1) & allValues(count2) & allValues(count2 + 1)
                                    value = value.Replace(expresion.Replace("-", " - ").Replace("+", " + "), Sum(expresion).Replace("-", " - "))
                                End If
                                If (allValues(count2 - 2) = "-" And allValues(count2 + 1) = "-") Then
                                    expresion = allValues(count2 - 2) & allValues(count2 - 1) & allValues(count2) & allValues(count2 + 1) & allValues(count2 + 2)
                                    value = value.Replace(expresion.Replace("-", " - ").Replace("+", " + ").Replace("  ", " "), " " & Sum(expresion))
                                End If
                                If (allValues(count2 + 1) = "-" And allValues(count2 - 2) <> "-") Then
                                    expresion = allValues(count2 - 1) & allValues(count2) & allValues(count2 + 1) & allValues(count2 + 2)
                                    value = value.Replace(expresion.Replace("-", " - ").Replace("+", " + ").Replace("  ", " "), Sum(expresion).Replace("-", "- "))
                                End If
                                If (allValues(count2 + 1) <> "-" And allValues(count2 - 2) <> "-") Then
                                    expresion = allValues(count2 - 1) & allValues(count2) & allValues(count2 + 1)
                                    value = value.Replace(expresion.Replace("+", " + "), Sum(expresion))
                                End If

                            ElseIf (allValues(count2) = "-" And num2 <= 0 And num1 <= 0 And count2 > 1) Then
                                If (allValues(count2 - 2) = "-" And allValues(count2 + 1) <> "-") Then
                                    expresion = allValues(count2 - 2) & allValues(count2 - 1) & allValues(count2) & allValues(count2 + 1)
                                    value = value.Replace(expresion.Replace("-", " - ").Replace("+", " + "), Mini(expresion).Replace("-", " - "))
                                End If
                                If (allValues(count2 - 2) = "-" And allValues(count2 + 1) = "-") Then
                                    newExpresoin = allValues(count2 - 2) & allValues(count2 - 1) & "+" & allValues(count2 + 2)
                                    expresion = allValues(count2 - 2) & allValues(count2 - 1) & allValues(count2) & allValues(count2 + 1) & allValues(count2 + 2)
                                    value = value.Replace(expresion.Replace("-", " - ").Replace("  ", " "), Sum(newExpresoin).Replace("-", "- "))
                                End If
                                If (allValues(count2 + 1) = "-" And allValues(count2 - 2) <> "-") Then
                                    newExpresoin = allValues(count2 - 1) & "+" & allValues(count2 + 2)
                                    expresion = allValues(count2 - 1) & allValues(count2) & allValues(count2 + 1) & allValues(count2 + 2)
                                    value = value.Replace(expresion.Replace("-", " - ").Replace("+", " + ").Replace("  ", " "), Sum(newExpresoin).Replace("-", "- "))
                                End If
                                If (allValues(count2 + 1) <> "-" And allValues(count2 - 2) <> "-") Then
                                    expresion = allValues(count2 - 1) & allValues(count2) & allValues(count2 + 1)
                                    value = value.Replace(expresion.Replace("-", " - "), Mini(expresion))
                                End If
                            End If
                        Next
                    Catch ex As Exception

                    End Try
                Next
                Console.Write("---> ")
                Console.WriteLine(value)
            Catch ex As NullReferenceException

            End Try
        Loop Until value.Trim.ToLower.Equals("exit")




    End Sub

    Function Sum(ByVal value As String) As String
        Dim result As String
        Dim num1 As Double
        Dim n1 As String
        Dim num2 As Double
        Dim n2 As String
        Dim count As Double = 0

        For Each valueOne As String In value
            count = count + 1
            If (valueOne.Equals("+") And count > 1) Then
                n1 = value.Substring(0, count - 1)
                num1 = Double.Parse(n1)
                n2 = value.Substring(count)
                num2 = Double.Parse(n2)
                num2 = num1 + num2
                result = Convert.ToString(num2)
            End If
        Next
        Return result
    End Function

    Function Mult(ByVal value As String) As String
        Dim result As String
        Dim num1 As Double = 0
        Dim n1 As String
        Dim num2 As Double = 0
        Dim n2 As String
        Dim count As Double = 0

        For Each valueOne As String In value
            count = count + 1
            If (valueOne.Equals("*") And count > 1) Then
                n1 = value.Substring(0, count - 1)
                num1 = Double.Parse(n1)
                n2 = value.Substring(count)
                num2 = Double.Parse(n2)
                num2 = num1 * num2
                result = Convert.ToString(num2)
            End If
        Next
        Return result
    End Function

    Function Div(ByVal value As String) As String
        Dim result As String
        Dim num1 As Double = 0
        Dim n1 As String
        Dim num2 As Double = 0
        Dim n2 As String
        Dim count As Double = 0

        For Each valueOne As String In value
            count = count + 1
            If (valueOne.Equals("/") And count > 1) Then
                n1 = value.Substring(0, count - 1)
                num1 = Double.Parse(n1)
                n2 = value.Substring(count)
                num2 = Double.Parse(n2)
                If (num2 = 0) Then

                End If
                num2 = num1 / num2
                result = Convert.ToString(num2)
            End If
        Next

        Return result
    End Function

    Function Expo(ByVal value As String) As String
        Dim result As String
        Dim num1 As Double = 0
        Dim n1 As String
        Dim num2 As Double = 0
        Dim n2 As String
        Dim count As Double = 0

        For Each valueOne As String In value
            count = count + 1
            If (valueOne.Equals("^") And count > 1) Then
                n1 = value.Substring(0, count - 1)
                num1 = Double.Parse(n1)
                n2 = value.Substring(count)
                num2 = Double.Parse(n2)
                num2 = num1 ^ num2
                result = Convert.ToString(num2)
            End If
        Next
        Return result
    End Function

    Function Mini(ByVal value As String) As String
        Dim result As String
        Dim num1 As Double = 0
        Dim n1 As String
        Dim num2 As Double = 0
        Dim n2 As String
        Dim count As Double = 0

        For Each valueOne As String In value
            count = count + 1
            If (valueOne.Equals("-") And count > 1) Then
                n1 = value.Substring(0, count - 1)
                num1 = Double.Parse(n1)
                n2 = value.Substring(count)
                num2 = Double.Parse(n2)
                num2 = num1 - num2
                result = Convert.ToString(num2)
            End If
        Next
        Return result
    End Function
End Module